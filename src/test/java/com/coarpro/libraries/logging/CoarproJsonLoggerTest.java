package com.coarpro.libraries.logging;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CoarproJsonLoggerTest {
    ICoarproLogger logger;
    Path logfilePath;

    @BeforeAll
    public void voidSetupLogger() throws CoarproLoggerException, IOException {
        String currentDirectory = System.getProperty("user.dir");
        String logFileString = String.format("%s/%s", currentDirectory, "coarproJsonLoggerTest.log");
        File logfile = new File(logFileString);

        /*
            * Delete the logfile for anticipation
            * of creating a new one so the tests
            * do not become false.
            */
        if (logfile.exists()) {
            logfile.delete();
        }



        /*
            * Build an instance with debug and tracing
            * enabled to capture all log messages.
            */
        this.logger = new CoarproJsonLogger.CoarproJsonLoggerBuilder()
                                                    .withLogFile(logfile)
                                                    .logDebugMessages(true)
                                                    .logTraceMessages(true)
                                                    .build();

        this.logfilePath = this.logger.getLogfile().toPath();
    }

    @Test
    void testLoggingInfoMessage() {
        String message = "test CoarproJsonLoggerClass info message";
        logger.logInfoMessage(message);
        assertNotEquals(-1, logger.getLogContents().indexOf(String.format("INFO: %s", message)));
    }

    @Test
    void testLoggingWarningMessage() {
        String message = "test CoarproJsonLoggerClass warning message";
        logger.logWarningMessage(message);
        assertNotEquals(-1, logger.getLogContents().indexOf(String.format("WARNING: %s", message)));
    }

    @Test
    void testLoggingErrorMessage() {
        String message = "test CoarproJsonLoggerClass error message";
        logger.logErrorMessage(message);
        assertNotEquals(-1, logger.getLogContents().indexOf(String.format("ERROR: %s", message)));
    }

    @Test
    void testLoggingFatalMessage() {
        String message = "test CoarproJsonLoggerClass fatal message";
        logger.logFatalMessage(message);
        assertNotEquals(-1, logger.getLogContents().indexOf(String.format("FATAL: %s", message)));
    }

    @Test
    void testLoggingSevereMessage() {
        String message = "test CoarproJsonLoggerClass severe message";
        logger.logSevereMessage(message);
        assertNotEquals(-1, logger.getLogContents().indexOf(String.format("SEVERE: %s", message)));
    }

    @Test
    void testLoggingDebugMessage() {
        String message = "test CoarproJsonLoggerClass debug message";
        logger.logDebugMessage(message);
        assertNotEquals(-1, logger.getLogContents().indexOf(String.format("DEBUG: %s", message)));
    }

    @Test
    void testLoggingTraceMessage() {
        String message = "test CoarproJsonLoggerClass trace message";
        logger.logTraceMessage(message);
        assertNotEquals(-1, logger.getLogContents().indexOf(String.format("TRACE: %s", message)));
    }

    /**
     * The method tests flushing content stored
     * in the list to the logfile specfied and then
     * verifying that all the content was successfully
     * written to the logfile.
     * @throws IOException
     */
    @Test
    void testFlushingContentToLogFile() throws IOException {
        // (1) Get a copy of the log contents
        List<String> logContents = logger.getLogContents();

        /*
         * Flush content to disk. It should then clear
         * the array.
         */
        logger.flush();

        // (2) Verify the list is now empty
        assertTrue(logger.getLogContents().isEmpty());

        // (3) Verify all content was written to the logfile.
        String writtenLogFileContent = Files.readString(logfilePath, StandardCharsets.UTF_8);

        for (String logContent : logContents) {
            assertTrue(writtenLogFileContent.contains(logContent));
        }
    }
}
