package com.coarpro.libraries.logging;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CoarproLoggerTest {
    ICoarproLogger logger;
    Path logfilePath;

    @BeforeAll
    public void voidSetupLogger() throws CoarproLoggerException {
        String currentDirectory = System.getProperty("user.dir");
        String logFileString = String.format("%s/%s", currentDirectory, "coarproLoggerTest.log");
        File logfile = new File(logFileString);

        /*
         * Delete the logfile for anticipation
         * of creating a new one so the tests
         * do not become false.
         */
        if (logfile.exists()) {
            logfile.delete();
        }


        /*
         * Build an instance with debug and tracing
         * enabled to capture all log messages.
         */
        this.logger = new CoarproLogger.CoarproLoggerBuilder()
                                                 .withLogFile(logfile)
                                                 .logDebugMessages(true)
                                                 .logTraceMessages(true)
                                                 .build();

        this.logfilePath = this.logger.getLogfile().toPath();

    }

    @Test
    void testLoggingInfoMessage() throws IOException {
        String message = "test CoarproLoggerClass info message";
        logger.logInfoMessage(message);
        assertTrue(Files.readString(logfilePath).contains(String.format("INFO: %s", message)));
    }


    @Test
    void testLoggingWarningMessage() throws IOException {
        String message = "test CoarproLoggerClass warning message";
        logger.logWarningMessage(message);
        assertTrue(Files.readString(logfilePath).contains(String.format("WARNING: %s", message)));
    }

    @Test
    void testLoggingErrorMessage() throws IOException {
        String message = "test CoarproLoggerClass error message";
        logger.logErrorMessage(message);
        assertTrue(Files.readString(logfilePath).contains(String.format("ERROR: %s", message)));
    }

    @Test
    void testLoggingFatalMessage() throws IOException {
        String message = "test CoarproLoggerClass fatal message";
        logger.logFatalMessage(message);
        assertTrue(Files.readString(logfilePath).contains(String.format("FATAL: %s", message)));
    }

    @Test
    void testLoggingSevereMessage() throws IOException {
        String message = "test CoarproLoggerClass severe message";
        logger.logSevereMessage(message);
        assertTrue(Files.readString(logfilePath).contains(String.format("SEVERE: %s", message)));
    }

    @Test
    void testLoggingDebugMessage() throws IOException {
        String message = "test CoarproLoggerClass info message";
        logger.logDebugMessage(message);
        assertTrue(Files.readString(logfilePath).contains(String.format("DEBUG: %s", message)));
    }

    @Test
    void testLoggingTraceMessage() throws IOException {
        String message = "test CoarproLoggerClass trace message";
        logger.logTraceMessage(message);
        assertTrue(Files.readString(logfilePath).contains(String.format("TRACE: %s", message)));
    }
}
