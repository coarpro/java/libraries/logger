package com.coarpro.libraries.logging;

/**
 * A generic error exception class for the Coarpro Logger.
 */
public final class CoarproLoggerException extends Exception {
    /**
     * Throws a Coarpro Logger exception. Provide the error
     * message to be thrown.
     * @param errorMessage
     */
    public CoarproLoggerException(final String errorMessage) {
        super(errorMessage);
    }
}
