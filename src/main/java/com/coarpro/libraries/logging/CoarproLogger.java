package com.coarpro.libraries.logging;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * A basic logger class that writes to a logfile stored
 * on disk.
 */
public final class CoarproLogger implements ICoarproLogger {
    /**
     * The logfile content is written to.
     */
    private final File logfile;

    /**
     * A FileWriter object.
     */
    private final FileWriter fileWriter;

    /**
     * Indicates if trace messages are
     * being logged.
     */
    private Boolean isTracing = false;

    /**
     * Indicates if debug messages are being
     * logged.
     */
    private Boolean isDebugging = false;


    private CoarproLogger(final CoarproLoggerBuilder builder) {
        this.logfile = builder.logfile;
        this.isTracing = builder.isTracing;
        this.isDebugging = builder.isDebugging;
        this.fileWriter = builder.fileWriter;
    }

    @Override
    public void logDebugMessage(final String message) {
        logMessage(String.format("DEBUG: %s", message));
    }

    @Override
    public void logDebugMessageAndDisplay(final String message) {
        logDebugMessage(message);
        System.out.println(message);
    }

    @Override
    public void logInfoMessage(final String message) {
        logMessage(String.format("INFO: %s", message));
    }

    @Override
    public void logWarningMessage(final String message) {
        logMessage(String.format("WARNING: %s", message));
    }

    @Override
    public void logWarningMessageAndDisplay(final String message) {
        logWarningMessage(message);
        System.out.println(message);
    }

    @Override
    public void logInfoMessageAndDisplay(final String message) {
        logInfoMessage(message);
        System.out.println(message);
    }

    @Override
    public void logTraceMessage(final String message) {
        logMessage(String.format("TRACE: %s", message));
    }

    @Override
    public void logTraceMessageAndDisplay(final String message) {
        logTraceMessage(message);
        System.out.println(message);
    }


    @Override
    public void logErrorMessage(final String message) {
        logMessage(String.format("ERROR: %s", message));
    }

    @Override
    public void logErrorMessageAndDisplay(final String message) {
        logErrorMessage(message);
        System.out.println(message);
    }

    @Override
    public void logFatalMessage(final String message) {
        logMessage(String.format("FATAL: %s", message));
    }

    @Override
    public void logFatalMessageAndDisplay(final String message) {
        logFatalMessage(message);
        System.out.println(message);
    }

    @Override
    public void logSevereMessage(final String message) {
        logMessage(String.format("SEVERE: %s", message));
    }

    @Override
    public void logSevereMessageAndDisplay(final String message) {
        logSevereMessage(message);
        System.out.println(message);
    }

    /**
     * The method writes a log message to the logfile
     * provided when an instance was created. If
     * writing the message fails, then the method will
     * pause and display an error message. It will continuoulsy
     * display this error message until the issues has been
     * resolved.
     */
    @Override
    public void logMessage(final String message) {
        Boolean shouldWrite = true;

        while (Boolean.TRUE.equals(shouldWrite)) {
            try {
                fileWriter.append(String.format("%s%n", message));
                fileWriter.flush();
                shouldWrite = false;
            } catch (IOException e) {
                StringBuilder messageBuilder = new StringBuilder();
                messageBuilder.append(
                    String.format(
                        "SEVERE: The application has failed to write to logfile '%s' due to the following error message: %n%n %s.%n%n",
                        logfile.getAbsolutePath(), e.getMessage()
                    )
                );

                messageBuilder.append("Resolve the error message above.");

                messageBuilder.append("The application will continue retry and display this message until the error has been resolved.");

                System.out.println(messageBuilder.toString());
            }
        }
    }

    @Override
    public void logMessageAndDisplay(final String message) {
        logMessage(message);
        System.out.println(message);
    }

    @Override
    public void flush() {
        /*
         * The method is empty because this class
         * immediately writes log messages to the
         * file as opposed to storing it in a variable.
         */
    }

    @Override
    public File getLogfile() {
        return this.logfile;
    }

    @Override
    public Boolean getIsTracing() {
        return this.isTracing;
    }

    @Override
    public Boolean getIsDebugging() {
        return this.isDebugging;
    }

    /**
     * This operation isnt supported with this basic logger.
     * This is more suitable for something like the CoarproJsonLogger.
     */
    @Override
    public List<String> getLogContents() {
        return new ArrayList<>();
    }

    public static final class CoarproLoggerBuilder {

        /**
         * The logfile content will be written to.
         */
        private File logfile;

        /**
         * Indicates if trace messages are being
         * logged.
         */
        private Boolean isTracing = false;

        /**
         * Indicates if debug messages are being
         * logged.
         */
        private Boolean isDebugging = false;

        /**
         * A FileWriter object.
         */
        private FileWriter fileWriter;

        /**
         * Pass a file object for the logfile.
         * @param logFile
         * @return CoarproLoggerBuilder
         */
        public CoarproLoggerBuilder withLogFile(final File logFile) {
            this.logfile = logFile;

            return this;
        }

        /**
         * Enables tracing messages to be logged.
         *
         * @param isTrace specify true to enable logging of
         * tracing messages or false to disable it.
         * @return CoarproLoggerBuilder
         */
        public CoarproLoggerBuilder logTraceMessages(final Boolean isTrace) {
            this.isTracing = isTrace;

            return this;
        }

        /**
         * Enable debug messages to be logged.
         *
         * @param isDebug specify true to enable logging of
         * debug messages or false to disable it.
         * @return CoarproLoggerBuilder
         */
        public CoarproLoggerBuilder logDebugMessages(final Boolean isDebug) {
            this.isDebugging = isDebug;

            return this;
        }

        /**
         * Builds an instance of CoarproLogger using parameters
         * passed rom the builder methods and then returns
         * an ICoarproLogger object.
         * @return ICoarproLogger
         * @throws CoarproLoggerException
         */
        public ICoarproLogger build() throws CoarproLoggerException {
            if (this.logfile == null) {
                throw new CoarproLoggerException("A logfile was not specified to write log events when building the Logger");
            }

            try {
                this.fileWriter = new FileWriter(this.logfile, StandardCharsets.UTF_8, true);
            } catch (IOException e) {
                throw new CoarproLoggerException(e.getMessage());
            }

            return new CoarproLogger(this);
        }
    }
}
