package com.coarpro.libraries.logging;

import java.io.File;
import java.util.List;

public interface ICoarproLogger {
    /**
     * Writes a debug message.
     * @param message
     */
    void logDebugMessage(String message);

    /**
     * Writes a debug messages and displays it
     * to the screen.
     * @param message
     */
    void logDebugMessageAndDisplay(String message);

    /**
     * Writes an info message.
     * @param message
     */
    void logInfoMessage(String message);

    /**
     * Writes an info message and displays it
     * to the screen.
     * @param message
     */
    void logInfoMessageAndDisplay(String message);

    /**
     * Writes a warning message.
     *
     * @param message
     */
    void logWarningMessage(String message);

    /**
     * Writes a warning message and then displays
     * it to the screen.
     * @param message
     */
    void logWarningMessageAndDisplay(String message);

    /**
     * Writes a trace message.
     * @param message
     */
    void logTraceMessage(String message);

    /**
     * Writes a trace message and displays it
     * to the screen.
     * @param message
     */
    void logTraceMessageAndDisplay(String message);

    /**
     * Writes an error message.
     * @param message
     */
    void logErrorMessage(String message);

    /**
     * Writes an error message and displays it to the screen.
     * @param message
     */
    void logErrorMessageAndDisplay(String message);

    /**
     * Writes a fatal message.
     * @param message
     */
    void logFatalMessage(String message);

    /**
     * Writes a fatal message and displays it to the screen.
     * @param message
     */
    void logFatalMessageAndDisplay(String message);

    /**
     * Writes a severe message.
     * @param message
     */
    void logSevereMessage(String message);

    /**
     * Writes a severe message and displays it to the screen.
     * @param message
     */
    void logSevereMessageAndDisplay(String message);


    /**
     * Writes a log message without prepending
     * "info", "debug", "trace".
     * @param message
     */
    void logMessage(String message);

    /**
     * Wrties a log message without prepending
     * "Info", "debug", "trace" and displalys it
     * to the screen.
     * @param message
     */
    void logMessageAndDisplay(String message);

    /**
     * Writes all content from cache, collection, array list into
     * the log file. For example if logs are being
     * stored into an array list, this method will write
     * all contents stored in the array list into the log file
     * with each index content being on its own line.
     *
     * NOTE: Logic should exist to ensure that if a log message
     * fails to get written that the method continuously retries
     * and displays a notification informing of the error that must
     * be resolved in order for the log message to be written.
     */
    void flush();

    /**
     * Returns the logfile that events are being written to.
     *
     * @return File
     */
    File getLogfile();

    /**
     * Returns true if tracing is enabled. returns false otherwise.
     *
     * @return Boolean
     */
    Boolean getIsTracing();

    /**
     * Returns true if debugging is enabled. returns false otherwise.
     * @return Boolean
     */
    Boolean getIsDebugging();

    /**
     * returns the contents of the logfile in array format.
     * @return Boolean
     */
    List<String> getLogContents();

}
