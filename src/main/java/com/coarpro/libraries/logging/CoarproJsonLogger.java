package com.coarpro.libraries.logging;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Create a JSON logger for event logging events
 * in JSON format. All log entries are written to
 * an array list. The array list can be be accessed
 * using a JSON parser such as the Jackson library.
 * or it can be used to return data in JSON format
 * via a REST API.
 */
public final class CoarproJsonLogger implements ICoarproLogger {

    /**
     *
     */
    private final File logfile;

    /**
     * Stores log messages.
     */
    private final List<String> logContents;

    /**
     * Object used to write objets.
     */
    private final FileWriter fileWriter;

    /**
     * Indicator to know if trace messages are being written.
     */
    private Boolean isTracing = false;

    /**
     * Indicator to know if debug messages are being written.
     */
    private Boolean isDebugging = false;

    /**
     * The instance is private to force creation using its
     * builder class.
     * @param builder - Pass an instance of CoarproJsonLoggerBuilder.
     */
    private CoarproJsonLogger(final CoarproJsonLoggerBuilder builder) {
        this.logfile = builder.logfile;
        this.isTracing = builder.isTracing;
        this.isDebugging = builder.isDebugging;
        this.logContents = builder.logContents;
        this.fileWriter = builder.fileWriter;
    }

    /**
     * It prepends "DEBUG: " to the message to indicate
     * this is a debug message and writes the message to
     * the file.
     *
     * @param message
     */
    @Override
    public void logDebugMessage(final String message) {
        logMessage(String.format("DEBUG: %s", message));
    }

    /**
     * It prepends "DEBUG: " to the message to indicate
     * this is a debug message and writes the message to
     * the logfile anad displays the message.
     *
     * @param message
     */
    @Override
    public void logDebugMessageAndDisplay(final String message) {
        logDebugMessage(message);
        System.out.println(message);
    }

    /**
     * It prepends "INFO: " to the message to indicate this
     * is an info message and writes the message to the
     * logfile.
     *
     * @param message
     */
    @Override
    public void logInfoMessage(final String message) {
       logMessage(String.format("INFO: %s", message));
    }

    /**
     * It prepends "INFO: " to the message to indicate this
     * is an info message and writes the message to the
     * logfile. It then displays the message.
     *
     * @param message
     */
    @Override
    public void logInfoMessageAndDisplay(final String message) {
        logInfoMessage(message);
        System.out.println(message);
    }

    @Override
    public void logWarningMessage(final String message) {
        logMessage(String.format("WARNING: %s", message));
    }

    @Override
    public void logWarningMessageAndDisplay(final String message) {
        logWarningMessage(message);
        System.out.println(message);
    }

    /**
     * It prepends "TRACE: " to the message to indicate this
     * is an trace message and writes the message to the
     * logfile.
     *
     * @param message
     */
    @Override
    public void logTraceMessage(final String message) {
        logMessage(String.format("TRACE: %s", message));
    }

    /**
     * It prepends "TRACE: " to the message to indicate this
     * is an trace message and writes the message to the
     * logfile. It then displays the message.
     *
     * @param message
     */
    @Override
    public void logTraceMessageAndDisplay(final String message) {
        logTraceMessage(message);
        System.out.println(message);
    }

    /**
     * It prepends "ERROR: " to the message to indicate this
     * is an error message and writes the message to the
     * logfile.
     *
     * @param message
     */
    @Override
    public void logErrorMessage(final String message) {
        logMessage(String.format("ERROR: %s", message));
    }

    /**
     * It prepends "ERROR: " to the message to indicate this
     * is an error message and writes the message to the
     * logfile. It then displays the message.
     *
     * @param message
     */
    @Override
    public void logErrorMessageAndDisplay(final String message) {
        logErrorMessage(message);
        System.out.println(message);
    }

    /**
     * It prepends "FATAL: " to the message to indicate this
     * is an fatal message and writes the message to the
     * logfile.
     *
     * @param message
     */
    @Override
    public void logFatalMessage(final String message) {
        logMessage(String.format("FATAL: %s", message));
    }

    /**
     * It prepends "FATAL: " to the message to indicate this
     * is a fatal message and writes the message to the
     * logfile. It then displays the message.
     *
     * @param message
     */
    @Override
    public void logFatalMessageAndDisplay(final String message) {
        logFatalMessage(message);
        System.out.println(message);
    }

    /**
     * It prepends "SEVERE: " to the message to indicate this
     * is a severe message and writes the message to the
     * logfile.
     *
     * @param message
     */
    @Override
    public void logSevereMessage(final String message) {
        logMessage(String.format("SEVERE: %s", message));
    }

    /**
     * It prepends "SEVERE: " to the message to indicate this
     * is a severe message and writes the message to the
     * logfile. It then displays the message.
     *
     * @param message
     */
    @Override
    public void logSevereMessageAndDisplay(final String message) {
        logSevereMessage(message);
        System.out.println(message);
    }

    /**
     * It writes a message to the logfile.
     *
     * @param message
     */
    @Override
    public void logMessage(final String message) {
        logContents.add(message);
    }

    /**
     * It writes a message to the logfile and
     * then displays the message.
     *
     * @param message
     */
    @Override
    public void logMessageAndDisplay(final String message) {
        logMessage(message);
        System.out.println(message);
    }

    /**
     * It writes all content stored in the array list
     * to the specified file and then clears the
     * array list.
     *
     */
    @Override
    public void flush() {

        Boolean shouldWrite = true;

        /*
         * Attempts to write all content to the logfile on the disk.
         * If an error occurs the program will attempt to retry to
         * write the mesage until the error has been resolved.
         */
        while (Boolean.TRUE.equals(shouldWrite)) {
            try {
                fileWriter.append("Request to log all content in the collection has been called.");

                for (String logContent : logContents) {
                    fileWriter.append(String.format("%s%n", logContent));
                }

                logContents.clear();

                fileWriter.append("Successfully flushed all content from the collection into this file.");

                /*
                 * Set to false so that it doesnt attempt to rewrite contents.
                 */
                shouldWrite = false;
            } catch (IOException e) {
                StringBuilder message = new StringBuilder();
                message.append(
                    String.format(
                        "SEVERE: The application has failed to write to logfile '%s' due to the following error message: %n%n %s.%n%n",
                        logfile.getAbsolutePath(), e.getMessage()
                    )
                );

                message.append("Resolve the error message above.");

                message.append("The application will continue retry and display this message until the error has been resolved.");
                System.out.println(message.toString());
            }
        }
    }

    /**
     * Returns the logfile that content
     * is being written to.
     */
    @Override
    public File getLogfile() {
        return logfile;
    }

    /**
     * Returns true if trace messages
     * are being logged. Returns false
     * otherwise.
     */
    @Override
    public Boolean getIsTracing() {
        return isTracing;
    }

    /**
     * Returns true if debug messages
     * are being logged. Returns false otherwise
     */
    @Override
    public Boolean getIsDebugging() {
        return isDebugging;
    }

    /**
     * Returns the log messages stored
     * in the list.
     */
    @Override
    public List<String> getLogContents() {
        /*
         * Return an unmodified list to prevent tampering
         */
        return Collections.unmodifiableList(logContents);
    }

    /**
     * Create the JSON logger for web events.
     */
    public static final class CoarproJsonLoggerBuilder {
        /**
         * The logfile content is written to.
         */
        private File logfile;

        /**
         * Indicates if trace messages should
         * be written.
         */
        private Boolean isTracing = false;

        /**
         * Indicates if debug messages should
         * be written.
         */
        private Boolean isDebugging = false;

        /**
         * Where log entries are written to.
         */
        private List<String> logContents = new ArrayList<>();

        /**
         * Filewriter object.
         */
        private FileWriter fileWriter;

        /**
         * Pass a File object that log messages should be written to.
         * @param logFile
         * @return CoarproJsonLoggerBuilder
         * @throws IOException
         */
        public CoarproJsonLoggerBuilder withLogFile(final File logFile) throws IOException {
            this.logfile = logFile;

            return this;
        }

        /**
         * Enables tracing messages to be logged.
         *
         * @param isTrace specify true to enable logging
         * of tracing messages or false to disable.
         * @return CoarproJsonLoggerBuilder
         */
        public CoarproJsonLoggerBuilder logTraceMessages(final Boolean isTrace) {
            this.isTracing = isTrace;

            return this;
        }

        /**
         * Enable debug messages to be logged.
         *
         * @param isDebug specify true to enable logging of
         * debug messages or false to disable it.
         * @return CoarproJsonLoggerBuilder
         */
        public CoarproJsonLoggerBuilder logDebugMessages(final Boolean isDebug) {
            this.isDebugging = isDebug;

            return this;
        }

        /**
         * Creates an instance of CoarproJsonLogger with parameters
         * passed to the build method and then returns the ICoarproLogger
         * Interface.
         * @return ICoarproLogger
         * @throws CoarproLoggerException
         */
        public ICoarproLogger build() throws CoarproLoggerException {
            if (this.logfile == null) {
                throw new CoarproLoggerException("A logfile was not specified to write log events when building the Logger");
            }

            try {
                this.fileWriter = new FileWriter(this.logfile, StandardCharsets.UTF_8, true);
            } catch (IOException e) {
                throw new CoarproLoggerException(e.getMessage());
            }

            return new CoarproJsonLogger(this);

        }
    }



}
