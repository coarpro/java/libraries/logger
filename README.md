# Coarpro Logger
The Coarpro logger is a basic standalone barebones library with minimal dependencies that can be used to create, and write messages to a logfile. Supported logging types include
* Logging to standard file
* Writing in JSON format for Rest API or compatibility with a JSON parser.

**NOTE**: At the present time the logger does not integrate with slf4j or log4j. It is very basic. Integration with these standard logging libraries will be included in a future update.

## Usage And Examples
All logger classes use a builder to create instances.

### Example: Standard Logger Example

The Standard logger just writes and appends to a logfile you specify.

This example assumes you have an application named "myapp" in the `/opt` directory.
```java
import com.coarpro.libraries.logging.CoarproLogger;

// (1) Create a new file object
File logFile = new File("/opt/myapp/logs/myapplog.log");

// (2) Create a logger instance.
ICoarproLogger logger = new CoarproLogger.CoarproLoggerBuilder()
                                        .withLogFile(logFile)
                                        .withTracingEnabled(false)
                                        .withDebugEnabled(false)
                                        .build()

// (3) Log message.
logger.logInfoMessage("Starting myapp");

```

### Example: JSON Logger Example
For stateless applications, micro service architecture, or webapps where you plan to provide server log messages that will be sent back and forth via API endpoints and be included in an application at the client site you can use the `CoarproJSONLogger`.
It will log information to a Java `ArrayList` so that it can be returned and parsed easily in `JSON` format.

```java
import com.coarpro.libraries.logging.CoarproJsonLogger;

// (1) Create a new file object
File logFile = new File("/opt/myapp/logs/myapplog.log");

// (2)
ICoarproLogger logger = new CoarproJsonLogger.CoarproJsonLoggerBuilder()
                                            .withLogFile(logFile)
                                            .withTracingEnabled(false)
                                            .withDebugEnabled(false)
                                            .build()

// (3) Write to the log
logger.logInfoMessage("Successfully authenticated to the WebApp");

/*
(4) If desired, flush log content to disk on the server side or client side. This will write all log messages
stored in the array list to the file specified in step 1. It will then clear
the array list.
*/
logger.flush();
```

Using an API endpoint, you can return activity from the server back to the applicaiton at the client site if desired. Assuming you are using Spring Framework, you can call the getter method to return the log contents which will return all data stored in the ArrayList.

```java
public List<String> returnLog() {
    List<String> logContent = logger.getLogContents();
    logger.flush();

    return logContent;
}
```

## Contributing/Bugs/Enhancement Requests
If contributing, please fork, make your changes and open a Merge Request. If you need to report a bug, or an enhancement then please create a new issue.