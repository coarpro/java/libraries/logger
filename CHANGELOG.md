# Changelog

## 2.0.0
- Changed methods of Builder methods `withDebugEnabled` and `withTracingEnabled` to accept Boolean values.
- Renamed methods `withDebugEnabled` and `withTracingEnabled` to `logDebugMessages` `logTraceMessages`
- Updated unit tests to reflect changes.

## 1.0.0
- Initial Release